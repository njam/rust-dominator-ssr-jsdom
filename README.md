rust-dominator-ssr-jsdom
========================

Rendering a WebAssembly [rust-dominator](https://github.com/Pauan/rust-dominator) application in NodeJS with JSDOM.

Usage
-----

Install dependencies
```
yarn install
```

### Running the application in a browser

Build the project and open it in your browser:
```
yarn run start
```

### Rendering the application in JSDOM with NodeJs

Build the wasm browser bundle
```
yarn run ssr-build
```

Render three different application states in JSDOM
```
yarn run ssr-render
```
```
3: <html><head></head><body><div><div>Counter: 3</div><button>Increase</button></div></body></html>
2: <html><head></head><body><div><div>Counter: 2</div><button>Increase</button></div></body></html>
1: <html><head></head><body><div><div>Counter: 1</div><button>Increase</button></div></body></html>
```

How it works
------------

1. The `src/` folder contains a simple WebAssembly application that displays a counter which can be increased by pressing a button. 
2. Using [rollup-plugin-rust](https://github.com/wasm-tool/rollup-plugin-rust) a browser bundle is created which exposes a JS function `createApp()` to instantiate and return the WebAssembly-application (see `ssr/rollup.config.js`).
3. A NodeJS script sets up a [jsdom](https://github.com/jsdom/jsdom) environment, loads the browser bundle, creates the application (by calling `createApp()`), and finally serializes the DOM tree and prints the resulting HTML (see `ssr/render.js`).
