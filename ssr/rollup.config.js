import rust from '@wasm-tool/rollup-plugin-rust'
import inject from '@rollup/plugin-inject'
import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import virtual from '@rollup/plugin-virtual'

export default {
  input: {
    index: 'entry'
  },
  output: {
    dir: 'dist/ssr',
    name: 'wasm_ssr',
    format: 'iife'
  },
  plugins: [
    virtual({
      entry: `
        import wasm from "./Cargo.toml"
        export async function createApp() {
          const module = await wasm()
          return module.create_app()
        }
      `,
    }),
    rust({
      debug: true,
      inlineWasm: true,
    }),

    // Polyfill `TextDecoder` which is not available in 'jsdom'.
    inject({
      TextDecoder: ['text-encoding', 'TextDecoder'],
      TextEncoder: ['text-encoding', 'TextEncoder']
    }),
    commonjs(),
    resolve(),
  ],
}
