import rust from '@wasm-tool/rollup-plugin-rust'
import html from '@rollup/plugin-html'
import serve from 'rollup-plugin-serve'
import virtual from '@rollup/plugin-virtual'

export default {
  input: 'entry',
  output: {
    dir: 'dist/serve',
    sourcemap: true,
  },
  plugins: [
    virtual({
      entry: `
        import wasm from "./Cargo.toml"
        export async function createApp() {
          const module = await wasm()
          return module.create_app()
        }
        createApp()
      `,
    }),
    rust({
      debug: true,
    }),
    html(),
    serve({
      contentBase: 'dist/serve',
      open: true,
    })
  ],
}
