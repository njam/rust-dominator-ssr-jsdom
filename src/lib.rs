use std::rc::Rc;

use dominator::{Dom, DomBuilder, events};
use futures_signals::signal::{Mutable, SignalExt};
use wasm_bindgen::prelude::*;

#[derive(Default)]
struct State {
    counter: Mutable<i32>,
}

impl State {
    fn render(self: &Rc<Self>) -> Dom {
        html("div")
            .children(&mut [
                html("div")
                    .text_signal(self.counter.signal().map(|x| format!("Counter: {}", x)))
                    .into_dom(),
                html("button")
                    .text("Increase")
                    .event({
                        let state = Rc::clone(self);
                        move |_: events::Click| {
                            state.counter.replace_with(|x| *x + 1);
                        }
                    })
                    .into_dom(),
            ])
            .into_dom()
    }
}

#[wasm_bindgen]
#[derive(Default)]
pub struct App {
    state: Rc<State>,
}

#[wasm_bindgen]
impl App {
    #[wasm_bindgen(constructor)]
    pub fn new() -> App {
        App::default()
    }

    #[wasm_bindgen]
    pub fn set_counter(&self, value: i32) {
        self.state.counter.set(value);
    }

    fn render(&self) -> Dom {
        self.state.render()
    }
}

#[wasm_bindgen]
pub fn create_app() -> App {
    console_error_panic_hook::set_once();

    let app = App::new();
    dominator::append_dom(&dominator::body(), app.render());

    app
}

fn html(name: &str) -> DomBuilder<web_sys::HtmlElement> {
    DomBuilder::new_html(name)
}
